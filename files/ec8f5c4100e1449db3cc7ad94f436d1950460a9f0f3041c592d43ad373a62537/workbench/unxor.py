#! /usr/bin/env python

import struct
f = open('../436625438', 'rb').read()
key = f[0x5a20:0x5a30]

[f[i] ^ key[i & 15] for i in range(len(f))]

b''.join([struct.pack('B', f[i] ^ key[i & 15]) for i in range(len(f))])
o = open('436625438_unxor.sample', 'wb')
o.write(b''.join([struct.pack('B', f[i] ^ key[i & 15])
                  for i in range(len(f))]))
o.close()
